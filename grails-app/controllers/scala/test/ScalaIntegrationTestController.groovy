package scala.test

class ScalaIntegrationTestController {

    def index(){
        redirect(action: 'objectIntegrationTest')
    }

    def objectIntegrationTest() {

        def factorials = [
                [name: '10',     value: ScalaObjectTest.factorial(10)],
                [name: '100',    value: ScalaObjectTest.factorial(100)],
                [name: '1000',   value: ScalaObjectTest.factorial(1000)]
        ]

        render(view:'index', model: [factorials : factorials])
    }


    def classIntegrationTest() {

        def factorialClass = new ScalaClassTest()

        def factorials = [
                [name: '10',     value: factorialClass.factorial(10)],
                [name: '100',    value: factorialClass.factorial(100)],
                [name: '1000',   value: factorialClass.factorial(1000)]
        ]

        render(view:'index', model: [factorials : factorials])
    }




}
