package scala.test

/**
 * Sample object to test with.
 * <br/><br/>
 * User: brad
 * Date: 11/30/2012 11:14 AM
 */
object ScalaObjectTest {


  def factorial(x: Int): BigInt = {
    return factorialTailRecursive(x, 1);
  }

  def factorial(x: BigInt): BigInt = {
    return factorialTailRecursive(x, 1);
  }

  def factorialTailRecursive(x: BigInt, result: BigInt): BigInt = {
    if (x==0)
      result
    else
      factorialTailRecursive(x-1, x*result)
  }

}
