package scala.test


class ScalaClassTest {

  def factorial(x: Int): BigInt = {
    return factorialTailRecursive(x, 1);
  }

  def factorial(x: BigInt): BigInt = {
    return factorialTailRecursive(x, 1);
  }

  def factorialTailRecursive(x: BigInt, result: BigInt): BigInt = {
    if (x==0)
      result
    else
      factorialTailRecursive(x-1, x*result)
  }

}
